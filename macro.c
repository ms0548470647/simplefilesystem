#include <stdio.h>

#define Area_Cube(x) ((x)*(x)*(x))


int main()
{
    int area = Area_Cube(3);
    printf("The Area is: %d", area);

    return 0;

}
